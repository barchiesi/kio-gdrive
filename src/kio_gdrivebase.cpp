/*
 * Copyright 2020 David Barchiesi <david@barchie.si>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kio_gdrivebase.h"
#include "gdriveurl.h"
#include "gdrivedebug.h"
#include "gdrivehelper.h"

#include <QStringLiteral>
#include <QUrlQuery>
#include <QEventLoop>

#include <KLocalizedString>
#include <KGAPI/Types>
#include <KGAPI/Drive/About>
#include <KGAPI/Drive/AboutFetchJob>
#include <KGAPI/Drive/File>
#include <KGAPI/Drive/FileSearchQuery>
#include <KGAPI/Drive/FileFetchJob>
#include <KGAPI/Drive/Drives>
#include <KGAPI/Drive/DrivesFetchJob>

using namespace KGAPI2;
using namespace Drive;

KIOGDriveBase::KIOGDriveBase()
{
    qCDebug(GDRIVE) << "KIOGDriveBase ctor" << this;
    m_accountManager.reset(new AccountManager);
    m_cache.reset(new PathCache);
}

KGAPI2::AccountPtr KIOGDriveBase::getAccount(const QString &accountName)
{
    return m_accountManager->account(accountName);
}

QString KIOGDriveBase::resolveFileIdFromPath(const QString &path, PathFlags flags)
{
    qCDebug(GDRIVE) << "Resolving file ID for" << path;

    if (path.isEmpty()) {
        return QString();
    }

    const QString fileId = m_cache->idForPath(path);
    if (!fileId.isEmpty()) {
        qCDebug(GDRIVE) << "Resolved" << path << "to" << fileId << "(from cache)";
        return fileId;
    }

    QUrl url;
    url.setScheme(GDriveUrl::Scheme);
    url.setPath(path);
    const auto gdriveUrl = GDriveUrl(url);
    Q_ASSERT(!gdriveUrl.isRoot());

    if (gdriveUrl.isAccountRoot() || gdriveUrl.isTrashDir()) {
        qCDebug(GDRIVE) << "Resolved" << path << "to account root";
        return rootFolderId(gdriveUrl.account());
    }

    if (gdriveUrl.isSharedDrive()) {
        // The gdriveUrl.filename() could be the Shared Drive id or
        // the name depending on whether we are navigating from a parent
        // or accessing the url directly, use the shared drive specific
        // solver to disambiguate
        return resolveSharedDriveId(gdriveUrl.filename(), gdriveUrl.account());
    }

    if (gdriveUrl.isSharedDrivesRoot()) {
        qCDebug(GDRIVE) << "Resolved" << path << "to Shared Drives root";
        return QString();
    }

    // Try to recursively resolve ID of parent path - either from cache, or by
    // querying Google
    const QString parentId = resolveFileIdFromPath(gdriveUrl.parentPath(), KIOGDriveBase::PathIsFolder);
    if (parentId.isEmpty()) {
        // We failed to resolve parent -> error
        return QString();
    }

    qCDebug(GDRIVE) << "Getting ID for" << gdriveUrl.filename() << "in parent with ID" << parentId;

    FileSearchQuery query;
    if (flags != KIOGDriveBase::None) {
        query.addQuery(FileSearchQuery::MimeType,
                       (flags & KIOGDriveBase::PathIsFolder ? FileSearchQuery::Equals : FileSearchQuery::NotEquals),
                       GDriveHelper::folderMimeType());
    }
    query.addQuery(FileSearchQuery::Title, FileSearchQuery::Equals, gdriveUrl.filename());
    query.addQuery(FileSearchQuery::Parents, FileSearchQuery::In, parentId);
    query.addQuery(FileSearchQuery::Trashed, FileSearchQuery::Equals, gdriveUrl.isTrashed());

    const QString accountId = gdriveUrl.account();
    FileFetchJob fetchJob(query, getAccount(accountId));
    fetchJob.setFields({File::Fields::Id, File::Fields::Title, File::Fields::Labels});
    if (!runJob(fetchJob, accountId)) {
        return QString();
    }

    const ObjectsList objects = fetchJob.items();
    if (objects.isEmpty()) {
        qCWarning(GDRIVE) << "Failed to resolve" << path;
        return QString();
    }

    const FilePtr file = objects[0].dynamicCast<File>();

    m_cache->insertPath(path, file->id());

    qCDebug(GDRIVE) << "Resolved" << path << "to" << file->id() << "(from network)";
    return file->id();
}

QString KIOGDriveBase::resolveSharedDriveId(const QString &idOrName, const QString &accountId)
{
    qCDebug(GDRIVE) << "Resolving shared drive id for" << idOrName;

    const auto idOrNamePath = GDriveUrl::buildSharedDrivePath(accountId, idOrName);
    QString fileId = m_cache->idForPath(idOrNamePath);
    if (!fileId.isEmpty()) {
        qCDebug(GDRIVE) << "Resolved shared drive id" << idOrName << "to" << fileId << "(from cache)";
        return fileId;
    }

    // We start by trying to fetch a shared drive with the filename as id
    DrivesFetchJob searchByIdJob(idOrName, getAccount(accountId));
    searchByIdJob.setFields({
        Drives::Fields::Kind,
        Drives::Fields::Id,
        Drives::Fields::Name
    });
    QEventLoop eventLoop;
    QObject::connect(&searchByIdJob, &KGAPI2::Job::finished,
                     &eventLoop, &QEventLoop::quit);
    eventLoop.exec();
    if (searchByIdJob.error() == KGAPI2::OK || searchByIdJob.error() == KGAPI2::NoError) {
        // A Shared Drive with that id exists so we return it
        const auto objects = searchByIdJob.items();
        const DrivesPtr sharedDrive = objects.at(0).dynamicCast<Drives>();
        fileId = sharedDrive->id();
        qCDebug(GDRIVE) << "Resolved shared drive id" << idOrName << "to" << fileId;

        const auto idPath = idOrNamePath;
        const auto namePath = GDriveUrl::buildSharedDrivePath(accountId, sharedDrive->name());
        m_cache->insertPath(idPath, fileId);
        m_cache->insertPath(namePath, fileId);

        return fileId;
    }

    // The gdriveUrl's filename is not a shared drive id, we must
    // search for a shared drive with the filename name.
    // Unfortunately searching by name is only allowed for admin
    // accounts (i.e. useDomainAdminAccess=true) so we retrieve all
    // shared drives and search by name here
    DrivesFetchJob sharedDrivesFetchJob(getAccount(accountId));
    sharedDrivesFetchJob.setFields({
        Drives::Fields::Kind,
        Drives::Fields::Id,
        Drives::Fields::Name
    });
    QObject::connect(&sharedDrivesFetchJob, &KGAPI2::Job::finished,
                     &eventLoop, &QEventLoop::quit);
    eventLoop.exec();
    if (sharedDrivesFetchJob.error() == KGAPI2::OK || sharedDrivesFetchJob.error() == KGAPI2::NoError) {
        const auto objects = sharedDrivesFetchJob.items();
        for (const auto &object : objects) {
            const DrivesPtr sharedDrive = object.dynamicCast<Drives>();

            // If we have one or more hits we will take the first as good because we
            // don't have any other measures for picking the correct drive
            if (sharedDrive->name() == idOrName) {
                fileId = sharedDrive->id();
                qCDebug(GDRIVE) << "Resolved shared drive id" << idOrName << "to" << fileId;

                const auto idPath = GDriveUrl::buildSharedDrivePath(accountId, fileId);
                const auto namePath = idOrNamePath;
                m_cache->insertPath(idPath, fileId);
                m_cache->insertPath(namePath, fileId);

                return fileId;
            }
        }
    }

    // We couldn't find any shared drive with that id or name
    qCDebug(GDRIVE) << "Failed resolving shared drive" << idOrName << "(couldn't find drive with that id or name)";
    return QString();
}

QString KIOGDriveBase::rootFolderId(const QString &accountId)
{
    auto it = m_rootIds.constFind(accountId);
    if (it == m_rootIds.cend()) {
        qCDebug(GDRIVE) << "Getting root ID for" << accountId;
        AboutFetchJob aboutFetch(getAccount(accountId));
        aboutFetch.setFields({About::Fields::Kind, About::Fields::RootFolderId});
        QUrl url;
        if (!runJob(aboutFetch, accountId)) {
            return QString();
        }

        const AboutPtr about = aboutFetch.aboutData();
        if (!about || about->rootFolderId().isEmpty()) {
            qCWarning(GDRIVE) << "Failed to obtain root ID";
            return QString();
        }

        auto v = m_rootIds.insert(accountId, about->rootFolderId());
        return *v;
    }

    return *it;
}

void KIOGDriveBase::execJob(KGAPI2::Job &job)
{
    qCDebug(GDRIVE) << "Running job" << (&job) << "with accessToken" << GDriveHelper::elideToken(job.account()->accessToken());
    QEventLoop eventLoop;
    QObject::connect(&job, &KGAPI2::Job::finished,
            &eventLoop, &QEventLoop::quit);
    eventLoop.exec();
}

bool KIOGDriveBase::runJob(KGAPI2::Job &job, const QString &accountId)
{
    Q_FOREVER {
        qCDebug(GDRIVE) << "Running job" << (&job) << "with accessToken" << GDriveHelper::elideToken(job.account()->accessToken());
        QEventLoop eventLoop;
        QObject::connect(&job, &KGAPI2::Job::finished,
                         &eventLoop, &QEventLoop::quit);
        eventLoop.exec();
        qCDebug(GDRIVE) << "Completed job" << (&job) << "error code:" << job.error() << "- message:" << job.errorString();

        const auto error = job.error();
        if (error == KGAPI2::OK || error == KGAPI2::NoError) {
            return true;
        } else if (error == KGAPI2::Unauthorized) {
            const KGAPI2::AccountPtr oldAccount = job.account();
            const KGAPI2::AccountPtr account = m_accountManager->refreshAccount(oldAccount);
            if (!account) {
                // error(KIO::ERR_CANNOT_LOGIN, url.toDisplayString());
                return false;
            }
        } else {
            // Could be KGAPI2::AuthCancelled KGAPI2::AuthError KGAPI2::Forbidden KGAPI2::NotFound KGAPI2::NoContent KGAPI2::QuotaExceeded
            return false;
        }

        job.setAccount(getAccount(accountId));
        job.restart();
    };

    return false;
}
