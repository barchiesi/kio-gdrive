/*
 * Copyright 2020 David Barchiesi <david@barchie.si>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOGDRIVEBASE_H
#define KIOGDRIVEBASE_H

#include "gdrivebackend.h"
#include "pathcache.h"

#include <memory>

#include <QUrl>

#include <KGAPI/Account>
#include <KGAPI/Job>


class KIOGDriveBase
{

    public:
        KIOGDriveBase();
        KGAPI2::AccountPtr getAccount(const QString &accountName);
        AbstractAccountManager *getAccountManager() { return m_accountManager.get(); };
        PathCache *getPathCache() { return m_cache.get(); };

        enum PathFlags {
            None = 0,
            PathIsFolder = 1,
            PathIsFile = 2
        };
        QString resolveFileIdFromPath(const QString &path, PathFlags flags = None);
        QString resolveSharedDriveId(const QString &idOrName, const QString &accountId);
        void execJob(KGAPI2::Job &job);
        bool runJob(KGAPI2::Job &job, const QString &accountId);
        QString rootFolderId(const QString &accountId);

    private:
        QMap<QString /* account */, QString /* rootId */> m_rootIds;
        std::unique_ptr<AbstractAccountManager> m_accountManager;
        std::unique_ptr<PathCache> m_cache;
};

#endif
