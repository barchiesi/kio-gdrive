/*
 * Copyright 2020 David Barchiesi <david@barchie.si>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EDITPERMISSIONDIALOG_H
#define EDITPERMISSIONDIALOG_H

#include "kio_gdrivebase.h"
#include "ui_editpermission.h"
#include <QDialog>


class EditPermissionDialog : public QDialog, KIOGDriveBase
{
    Q_OBJECT

    public:
        explicit EditPermissionDialog(const KGAPI2::Drive::PermissionPtr permission, const QString fileId, const KGAPI2::AccountPtr account, QWidget *parent = nullptr);
        explicit EditPermissionDialog(const KGAPI2::Drive::PermissionPtr permission, const QUrl url, QWidget *parent = nullptr);
        explicit EditPermissionDialog(const QString fileId, const KGAPI2::AccountPtr account, QWidget *parent = nullptr);
        explicit EditPermissionDialog(const QUrl url, QWidget *parent = nullptr);
        ~EditPermissionDialog() override;

    Q_SIGNALS:
        void permissionsModified();

    private Q_SLOTS:
        void slotNotifyCheckChanged(int state);
        void slotMessageCheckChanged(bool checked);
        void slotDialogAccepted();
        void slotAddPermissionFinished(KGAPI2::Job *job);

    private:
        explicit EditPermissionDialog(QWidget *parent = nullptr);
        void setupModifyingUi(const KGAPI2::Drive::PermissionPtr permission);

        Ui::EditPermissionDialog *m_ui;
        KGAPI2::AccountPtr account;
        bool isNewPermission;
        QString fileId;
        QString permissionId;
};

#endif
