/*
 * Copyright 2020 David Barchiesi <david@barchie.si>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "editpermissiondialog.h"
#include "gdriveurl.h"
#include "gdrivedebug.h"

#include <QMessageBox>

#include <KLocalizedString>
#include <KGAPI/Drive/Permission>
#include <KGAPI/Drive/PermissionCreateJob>
#include <KGAPI/Drive/PermissionModifyJob>

EditPermissionDialog::EditPermissionDialog(const KGAPI2::Drive::PermissionPtr permission, const QString fileId, const KGAPI2::AccountPtr account, QWidget *parent)
    : EditPermissionDialog(fileId, account, parent)
{
    isNewPermission = false;
    permissionId = permission->id();
    setupModifyingUi(permission);
}

EditPermissionDialog::EditPermissionDialog(const KGAPI2::Drive::PermissionPtr permission, const QUrl url, QWidget *parent)
    : EditPermissionDialog(url, parent)
{
    isNewPermission = false;
    permissionId = permission->id();
    setupModifyingUi(permission);
}

EditPermissionDialog::EditPermissionDialog(const QString fileId, const KGAPI2::AccountPtr account, QWidget *parent)
    : EditPermissionDialog(parent)
{
    isNewPermission = true;
    this->fileId = fileId;
    this->account = account;
}

EditPermissionDialog::EditPermissionDialog(const QUrl url, QWidget *parent)
    : EditPermissionDialog(parent)
{
    isNewPermission = true;
    fileId = resolveFileIdFromPath(url.adjusted(QUrl::StripTrailingSlash).path(), KIOGDriveBase::None);
    const auto gdriveUrl = GDriveUrl(url);
    const QString accountId = gdriveUrl.account();
    account = getAccount(accountId);
}

EditPermissionDialog::EditPermissionDialog(QWidget *parent)
    : QDialog(parent),
    m_ui(new Ui::EditPermissionDialog)
{
    m_ui->setupUi(this);

    m_ui->roleCombo->insertItem(0, i18n("Reader"), KGAPI2::Drive::Permission::Role::ReaderRole);
    m_ui->roleCombo->insertItem(1, i18n("Writer"), KGAPI2::Drive::Permission::Role::WriterRole);
    m_ui->roleCombo->insertItem(2, i18n("Owner"), KGAPI2::Drive::Permission::Role::OwnerRole);
    m_ui->roleCombo->insertItem(3, i18n("Organizer"), KGAPI2::Drive::Permission::Role::OrganizerRole);
    m_ui->roleCombo->insertItem(4, i18n("File Organizer"), KGAPI2::Drive::Permission::Role::FileOrganizerRole);

    m_ui->typeCombo->insertItem(0, i18n("User"), KGAPI2::Drive::Permission::Type::TypeUser);
    m_ui->typeCombo->insertItem(1, i18n("Group"), KGAPI2::Drive::Permission::Type::TypeGroup);
    m_ui->typeCombo->insertItem(2, i18n("Domain"), KGAPI2::Drive::Permission::Type::TypeDomain);
    m_ui->typeCombo->insertItem(3, i18n("Anyone"), KGAPI2::Drive::Permission::Type::TypeAnyone);

    connect(m_ui->notifyCheckBox, &QCheckBox::stateChanged, this, &EditPermissionDialog::slotNotifyCheckChanged);
    connect(m_ui->shareMessageCheck, &QCheckBox::stateChanged, this, &EditPermissionDialog::slotMessageCheckChanged);
    connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this, &EditPermissionDialog::slotDialogAccepted);
}


EditPermissionDialog::~EditPermissionDialog()
{
    delete m_ui;
}


void EditPermissionDialog::setupModifyingUi(const KGAPI2::Drive::PermissionPtr permission)
{
    m_ui->permissionValueLineEdit->setText(permission->emailAddress());
    m_ui->permissionValueLineEdit->setEnabled(false);

    int index = m_ui->roleCombo->findData(permission->role());
    if ( index != -1 ) {
        m_ui->roleCombo->setCurrentIndex(index);
    }

    m_ui->typeCombo->setEnabled(false);
    m_ui->notifyCheckBox->setChecked(false);
    m_ui->notifyCheckBox->setEnabled(false);
    m_ui->shareMessageCheck->setEnabled(false);
}


void EditPermissionDialog::slotNotifyCheckChanged(int state)
{
    bool checked = state == Qt::Checked;
    m_ui->shareMessageCheck->setEnabled(checked);
}


void EditPermissionDialog::slotMessageCheckChanged(bool checked)
{
    m_ui->shareMessageTextEdit->setEnabled(checked);
}


void EditPermissionDialog::slotDialogAccepted()
{
    const QString email = m_ui->permissionValueLineEdit->text();
    const KGAPI2::Drive::Permission::Role role = static_cast<KGAPI2::Drive::Permission::Role>(m_ui->roleCombo->currentData().toInt());
    const KGAPI2::Drive::Permission::Type type = static_cast<KGAPI2::Drive::Permission::Type>(m_ui->typeCombo->currentData().toInt());
    const bool notifyUser = m_ui->notifyCheckBox->isChecked();
    const bool withNotificationMessage = m_ui->shareMessageCheck->isChecked();

    qCDebug(GDRIVE) << "Adding permission for" << email << "role" << role << "type" << type << (notifyUser ? "with" : "without") << "notification";

    KGAPI2::Drive::PermissionPtr permission = KGAPI2::Drive::PermissionPtr::create();
    permission->setValue(email);
    permission->setRole(role);
    permission->setType(type);

    KGAPI2::Job *job;
    if (isNewPermission) {
        KGAPI2::Drive::PermissionCreateJob *createJob = new KGAPI2::Drive::PermissionCreateJob(fileId, permission, account, this);
        createJob->setSendNotificationEmails(notifyUser);
        if (notifyUser && withNotificationMessage) {
            createJob->setEmailMessage(m_ui->shareMessageTextEdit->toPlainText());
        }
        job = createJob;
    } else {
        permission->setId(permissionId);
        KGAPI2::Drive::PermissionModifyJob *modifyJob = new KGAPI2::Drive::PermissionModifyJob(fileId, permission, account, this);
        if (role == KGAPI2::Drive::Permission::Role::OwnerRole) {
            modifyJob->setTransferOwnership(true);
        }
        job = modifyJob;
    }
    connect(job, &KGAPI2::Job::finished,
            this, &EditPermissionDialog::slotAddPermissionFinished);
}


void EditPermissionDialog::slotAddPermissionFinished(KGAPI2::Job *job)
{
    job->deleteLater();

    if (job->error() != KGAPI2::NoError) {
        qCDebug(GDRIVE) << "Failed adding permission" << job->errorString();
        QMessageBox::critical(this, i18n("Error"), job->errorString());
        return;
    }

    emit permissionsModified();
    accept();
}
